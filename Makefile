all: main sudoku

main: main.cpp Puzzle.h
	g++ -g -Wall main.cpp -o main

sudoku: sudoku.cpp Puzzle.h
	g++ -g -Wall sudoku.cpp -o sudoku

clean:
	rm main sudoku
