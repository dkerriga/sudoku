// Driver program for sudoku Puzzle. Part 1.
#include "Puzzle.h"

int main(void){
    // instantiate Puzzle objects
    Puzzle<int> sudoku("sudoku.txt");
    Puzzle<char> wordoku("wordoku.txt");
    // Print them out
    cout << "Sudoku:" << endl;
    sudoku.display();
    cout << endl << "Wordoku: " << endl;
    wordoku.display();
    cout << endl;
    return 0;
}
