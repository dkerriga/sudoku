// Daniel Kerrigan
// Lab 5
// Sudoku Puzzle class
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <fstream>

using namespace std;

template <typename T>
class Puzzle{
    public:
        Puzzle(string filename); // constructor that reads in file
        void display(); // print the board
        bool isSolved(); // returns true if the puzzle is solved
        bool add(T value, int row, int col); // add value to puzzle
    private:
        // a row, column or mini grid is valid if it does not contain
        // duplicates, other than 0.
        bool isValidRow(int row);
        bool isValidCol(int col);
        // row and col must represent the top left corner of the mini grid
        bool isValidMiniGrid(int row, int col);
        //  returns true if the vector has no duplicates
        bool isVectorUnique(vector<T>);
        vector<vector<T> > puzzle; // the puzzle that values are added to
        vector<vector<T> > original; // the orignal puzzle that was read
};
// constructor
template <typename T>
Puzzle<T>::Puzzle(string filename){
    // open the file
    ifstream infile;
    infile.open(filename.c_str());
    T value;
    // add each character to the puzzle 2d vector
    for(int i = 0; i < 9; i++){
        puzzle.push_back(vector<T>());
        for(int j = 0; j < 9; j++){
            infile >> value;
            puzzle[i].push_back(value);
        }
    }
    original = puzzle;
}
// print the puzzle with formatting
template <typename T>
void Puzzle<T>::display(){
    for(int i = 0; i < 9; i++){
        if(i % 3 == 0){
            cout << "|-----------|"<<endl;
        }
        for(int j = 0; j < 9; j++){
            if(j%3 == 0){
                cout << "|";
            }
            cout << puzzle[i][j];
        }
        cout << "|" << endl;
    }
    cout << "|-----------|" << endl << endl;
}
// adds the value to the square specified by row and col
// returns true if the value was added, false otherwise
template <typename T>
bool Puzzle<T>::add(T value, int row, int col ){
    // check for range of value
    if(value < 1 || value > 9){
        cout << "Invalid value: " << value << endl;
    }
    // check for range of row and column 
    else if(row < 0 || row > 8 || col < 0 || col > 8){
        cout << "Row and Column must be between 1 and 9." << endl;
    }
    // check if this square had no value in the original puzzle 
    else if(original[row][col] == 0){
        // save the previous value in this square
        T previousValue = puzzle[row][col];
        // assign the new value
        puzzle[row][col] = value;
        // check if the row, column, and mini grid do not contain duplicates
        // the 3*(row/3) and 3*(col/3) gets the top left corner
        // of the mini grid 
        if(isValidRow(row) &&  isValidCol(col) && 
                isValidMiniGrid(3*(row/3), 3*(col/3))){
            return true;
        }
        // if there are duplicates, then change back to previous value 
        else {
            cout << "Invalid placement." << endl;
            puzzle[row][col] = previousValue; 
        }
    } else {
        cout << "You cannot change the original value." << endl;
    }
    return false;
}
// returns true if the puzzle is solved
template <typename T>
bool Puzzle<T>::isSolved(){
    for(int i = 0; i < 9; i++){
        // every row and column must be valid
        if(!(isValidRow(i) && isValidCol(i))){
            return false;        
        }
        for(int j = 0; j < 9; j++){
            // there can be no 0s
            if(puzzle[i][j] == 0){
                return false;
            }
            // every minigrid must be valid
            if(i%3 == 0 && j%3 == 0 && !isValidMiniGrid(i,j)){
                return false;
            }
        }
    }
    return true;
}
// returns true if the row does not contain duplicates other than 0
template <typename T>
bool Puzzle<T>::isValidRow(int row){
    vector<T> values;
    for(int col = 0; col < 9; col++){
        T val = puzzle[row][col];
        if(val != 0){
            values.push_back(val);
        }
    }
    return isVectorUnique(values);
}

// returns true if the col does not contain duplicates other than 0
template <typename T>
bool Puzzle<T>::isValidCol(int col){
    vector<T> values;
    for(int row = 0; row < 9; row++){
        T val = puzzle[row][col];
        if(val != 0){
            values.push_back(val);
        }
    }
    return isVectorUnique(values);
}

// returns true if the mini grid does not contain duplicates other than 0
template <typename T>
bool Puzzle<T>::isValidMiniGrid(int row, int col){
    vector<T> values;
    for(int r = row; r < row+3; r++){
        for(int c = col; c < col+3; c++){
            T value = puzzle[r][c];
            if(value != 0){
                values.push_back(value);
            }
        }
    }
    return isVectorUnique(values);
}
 // returns true if there are no duplicates in the vector
template <typename T>
bool Puzzle<T>::isVectorUnique(vector<T> v){
    sort(v.begin(), v.end()); // sort
    bool isUnique = unique(v.begin(), v.end()) == v.end(); // check if unique
    return isUnique;
}
