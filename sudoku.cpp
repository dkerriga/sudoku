// Sudoku game
#include "Puzzle.h"
int main(void){
    // get file
    string filename;
    cout << "Enter file name: ";
    cin >> filename;
    // instantiate Puzzle object
    Puzzle<int> sudoku(filename);
    int row, col, value;
    // play the game until the puzzle is solved
    while(!sudoku.isSolved()){
        sudoku.display();
        cout << "Enter row (1-9): ";
        cin >> row;
        cout << "Enter column (1-9): ";
        cin >> col;
        cout << "Enter value (1-9): ";
        cin >> value;
        
        sudoku.add(value, row-1, col-1);
        
        cout << endl;
    }
    cout << "Congrats! You solved it!" << endl;
    sudoku.display();
    return 0;
}
